//
//  main.cpp
//  EP2_VARIABLE
//
//  Created by Nguyen Dung on 10/30/15.
//  Copyright © 2015 Nguyen Dung. All rights reserved.
//

#include <iostream>
#include <string>
using namespace std;

/*
    khai báo hàm trong C++
        + kiểu dữ liệu trả về 
        + tên hàm 
        + tham số truyền vào
    chú ý , hàm trong C cần có 1 nguyên mẫu hàm ở trên đầu file có tác dụng biên dịch hoặc gọi hàm , giống như hàm được định nghĩa ở file .h trong ObjectiveC
 */
/*
    biến trong C++
     + biến trong C++ gồm có -- int , char , short , long , float , double ,string
     + khai báo biến
        - kiểu dữ liệu -> tên biến , giá trị mặc định
     + biến string được sử dụng ở name space std; nếu bạn không khai báo ở đầu file thì cần phải sự dụng tường mình cho nó ví dụng str::std
 */

#define LINKUR "http://google.com"

void variable();

void stringCompare(string a,string b);
void convertStringToChar(string a);
void appenString(string a,string b);
void countString(string a);
void subString(string a,int start,int limit);


int main(int argc, const char * argv[]) {
    printf("Hellp word!\n");
    variable();
    return 0;
}
void variable(){
    printf("Fist Function\n");
//    int a = 1;
//    char b = 5;
//    short c = 0;
//    long d = 5000.0;
//    float e = 20.0;
//    double f = 20.00005;
    string str1 = "Nguyen Tien Dung";
    std::string str2 = " VCCORP";
    cout << str1 << "\n";
    cout << str2 << "\n";
    stringCompare(str2, str2);
    convertStringToChar(str1);
    appenString(str1, str2);
    countString(str1);
    const string testA = "123123123123123123";
    subString(testA, 5, 5);
    subString(LINKUR, 5, 5);
}
// so sánh chuỗi , nếu 2 chuỗi sau khi compare trả về = 0 là 2 chuỗi giống nhau
void stringCompare(string a,string b){
    cout << a.compare(b) << "\n";
    if((a.compare(b) >= 0)){
        cout << "true\n";
    }
    else{
        cout <<  "false\n";
    }
    if (a == b){
        cout << "true\n";
    }
}
// chuyển ký tự kiểu chuỗi sang kiểu char sử dụng hàm c_str()
void convertStringToChar(string a){
    cout << a.c_str();
}
// cộng chuỗi
void appenString(string a,string b){
    cout << "\n" << a + b << "\n";
}
// kiểm tra độ dài của chuỗi
void countString(string a){
    cout << a.length() << "\n";
}
// lấy 1 chuỗi con của 1 chuỗi
void subString(string a,int start,int limit){
    cout << a.substr(start,limit) << "\n";
}